import sys
import math
import operator

DEBUG = True

PARTS = {'BLUEBERRIES', 'ICE_CREAM', 'CHOPPED_STRAWBERRIES', 'GO'}

class BaseItems(object):
    def __init__(self, itemDescription):
        self.items = itemDescription.split('-')
        self.dish = "DISH" in self.items
        self.blue = "BLUEBERRIES" in self.items
        self.ice = "ICE_CREAM" in self.items
        self.chopped_straw = "CHOPPED_STRAWBERRIES" in self.items
        self.straw = "STRAWBERRIES" in self.items

    def __str__(self):
        return "Items: " + " ".join([item for item in self.items])

class PlayerItems(BaseItems):
    def __init__(self, stringCode):
        super(PlayerItems, self).__init__(stringCode)
        self.isNone = stringCode == "NONE"
        self.isEmptyDish = stringCode == "DISH"

    def get_components_count(self):
        return len(set(self.items).intersection(PARTS))

class AbstractObject(object):
    def __init__(self, raw_str):
        x, y, items_str = raw_str.split()
        self.x = int(x)
        self.y = int(y)
        self.items_str = items_str

class Player(AbstractObject):
    def __init__(self, raw_str):
        super(Player, self).__init__(raw_str)
        self.status = None
        self.items = PlayerItems(self.items_str)

    def __str__(self):
        return "Player: %d %d %s" % (self.x, self.y, self.items)

    def get_status(self):
        if self.items.isNone:
            return 'FREE'
        if self.items.isEmptyDish:
            return 'EMPTY'
        

class Table(AbstractObject):
    def __init__(self, table_info):
        super(Table, self).__init__(table_info)
        self.items = BaseItems(self.items_str)

    def __str__(self):
        return "Table: %d %d %s" % (self.x, self.y, self.items)

class Customer:
    def __init__(self, raw_str):
        order_str, award = raw_str.split()
        self.award = int(award)
        self.order = BaseItems(order_str)

    def __str__(self):
        return "Customer: %d %s" % (self.award, str(self.order))


class Customers:
    def __init__(self):
        self.customers = []

    def add_customer(self, raw_str):
        self.customers.append(Customer(raw_str))

    def __str__(self):
        ret = []
        for customer in self.customers:
            ret.append(str(customer))
        return "\n".join(ret)

    def get_oder_component(self, player_items):
        for customer in self.customers:
            isGood = True
            for item in player_items:
                if customer.order.items[i] != item:
                    isGood=False
                    break
            if isGood:
                if len(customer.order.items) == len(player_items):
                    return "GO"
                else:
                    return customer.order.items[len(player_items)]
        return "GO"

    def get_new_order(self):
        return self.get_Nth_components(0)

    def get_Nth_components(self, cnt):
        components = {}
        for cust in self.customers:
            component = cust.order.items[cnt+1]
            if component in components:
                components[component] += cust.award
            else:
                components[component] = cust.award
            if DEBUG:
                print >> sys.stderr, "selected customer component: " + component + " from " + str(cust.order.items)
        
        return max(components.iteritems(), key=operator.itemgetter(1))[0]

class Tables:
    def __init__(self):
        self.tables = []

    def add_table_with_item(self, table_info):
        self.tables.append(Table(table_info))

    def __str__(self):
        ret = []
        for table in self.tables:
            ret.append(str(table))
        return "\n".join(ret)


class Kitchen():
    def __init__(self, lines):
        self.lines = lines
        self.dish_coord = self.window_coord = self.blue_coord = self.ice_coord = self.straw_coord = self.chop_coord = None
        for i, kitchen_line in enumerate(lines):
            if self.dish_coord is None and "D" in kitchen_line:
                self.dish_coord = "%d %d" % (kitchen_line.index('D'), i)
            if self.window_coord is None and "W" in kitchen_line:
                self.window_coord = "%d %d" % (kitchen_line.index('W'), i)
            if self.blue_coord is None and "B" in kitchen_line:
                self.blue_coord = "%d %d" % (kitchen_line.index('B'), i)
            if self.ice_coord is None and "I" in kitchen_line:
                self.ice_coord = "%d %d" % (kitchen_line.index('I'), i)
            if self.straw_coord is None and "S" in kitchen_line:
                self.straw_coord = "%d %d" % (kitchen_line.index('S'), i)
            if self.chop_coord is None and "C" in kitchen_line:
                self.chop_coord = "%d %d" % (kitchen_line.index('C'), i)

    def __str__(self):
        ret = []
        ret.extend(self.lines)
        ret.append("Dish coords: " + self.dish_coord)
        ret.append("window_coord : " + self.window_coord)
        ret.append("blue_coord : " + self.blue_coord)
        ret.append("ice_coord: " + self.ice_coord)
        ret.append("straw_coord: " + self.straw_coord)
        return "\n".join(ret)

class Manager():
    def __init__(self, kitchen):
        self.kitchen = kitchen
        self.player = self.partner = self.customers = self.tables = None

    def fill(self, player, partner, customers, tables):
        self.player = player
        self.partner = partner
        self.customers = customers
        self.tables = tables

    def decide(self):
        #if self.player.completeReceipt():
        #    return "USE " + self.kitchen.window_coord
        if self.player.items.straw:
            return "USE " + self.kitchen.chop_coord
        print >> sys.stderr, "1"
        if self.player.items.chopped_straw and not self.player.items.dish:
            zanachka = self.getAppropriateEmptyTable(self.player, self.kitchen)
            manager.zanachka = zanachka
            return "USE " + zanachka
        print >> sys.stderr, "2"
        # components_count = self.player.items.get_components_count()
        # print >> sys.stderr, str(components_count)
        if self.player.items.isNone:
            print >> sys.stderr, "isNone"
            target = self.customers.get_new_order()
            target_coor = self.part_to_coord(target)
            return "USE " + target_coor

        if not self.player.items.dish:
            print >> sys.stderr, "use dish"
            return "USE " + self.kitchen.dish_coord
        print >> sys.stderr, "3"
        target = self.customers.get_oder_component(self.player.items)
        target_coor = self.part_to_coord(target)
        return "USE " + target_coor

    def getAppropriateEmptyTable(self, player, kitchen):
        x = player.x
        y = player.y
        print >> sys.stderr, "player coords %d %d" % (x, y)
        for i in xrange(-1,2):
            for j in xrange(-1,2):
                print >> sys.stderr, "kitchen %d %d: %s" % (x+i, y+j, kitchen.lines[y+i][x+j])
                if kitchen.lines[y+i][x+j] == "#":
                    return "%d %d" % (y+i, x+j)
        
        return "appropriate table not found! %d %d" % (x, y)

    def part_to_coord(self, part_name):
        if part_name not in PARTS:
            print >> sys.stderr, "strange part_name: " + part_name
        if part_name == 'GO':
            return self.kitchen.window_coord

        if part_name == 'BLUEBERRIES':
            return self.kitchen.blue_coord

        if part_name == 'ICE_CREAM':
            return self.kitchen.ice_coord

        if part_name == 'CHOPPED_STRAWBERRIES':
            return self.kitchen.straw_coord
            
        return

    def __str__(self):
        ret = []
        ret.append(str(self.kitchen))
        ret.append(str(self.customers))
        ret.append(str(self.player))
        ret.append(str(self.partner))
        ret.append(str(self.tables))
        return "\n".join(ret)

num_all_customers = int(raw_input())
customers = Customers()
for i in xrange(num_all_customers):
    customers.add_customer(raw_input())

kitchen_lines = []
for i in xrange(7):
    kitchen_lines.append(raw_input())
manager = Manager(Kitchen(kitchen_lines))

# game loop

while True:
    turns_remaining = int(raw_input())
    player = Player(raw_input())
    partner = Player(raw_input())

    num_tables_with_items = int(raw_input())  # the number of tables in the kitchen that currently hold an item
    tables = Tables()
    for i in xrange(num_tables_with_items):
        tables.add_table_with_item(raw_input())

    # oven_contents: ignore until wood 1 league
    oven_contents, oven_timer = raw_input().split()
    oven_timer = int(oven_timer)

    num_all_customers = int(raw_input())
    customers = Customers()
    for i in xrange(num_all_customers):
        customers.add_customer(raw_input())
    manager.fill(player, partner, customers, tables)
    # if (isFull):
    #     print "USE " + window_coord
    #     continue
    # if (isIEmpty):
    #     print "USE " + dish_coord        
    #     continue
    # if (isIhaveEmptyDish):
    #     print "USE " + blue_coord        
    #     continue
    # if (isIHaveBlue):
    #     print "USE " + ice_coord        
    #     continue
    print >> sys.stderr, manager
    print manager.decide()
    continue